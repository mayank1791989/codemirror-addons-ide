/* @flow */
module.exports = {
  plugins: ['playlyfe'],

  extends: [
    'plugin:playlyfe/js',
    'plugin:playlyfe/flowtype',
    'plugin:playlyfe/prettier',
  ],

  env: {
    browser: true,
  },

  rules: {
    'arrow-paren': 'off',
    'no-negated-condition': 'off',
  },
};
