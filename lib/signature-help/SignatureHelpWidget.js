'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _dom = require('../_utils/dom');

var dom = _interopRequireWildcard(_dom);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SignatureHelpWidget = function () {
  function SignatureHelpWidget() {
    (0, _classCallCheck3.default)(this, SignatureHelpWidget);
    this.state = {
      isVisible: false,
      help: null
    };

    this._createContainerElements();
  }

  (0, _createClass3.default)(SignatureHelpWidget, [{
    key: 'show',
    value: function show(help) {
      this.setState({
        isVisible: true,
        help: help
      });
    }
  }, {
    key: 'hide',
    value: function hide() {
      this.setState({
        isVisible: false,
        help: null
      });
    }
  }, {
    key: 'getElement',
    value: function getElement() {
      return this.rootEl;
    }
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          help = _state.help,
          isVisible = _state.isVisible;

      if (!isVisible || !help) {
        dom.removeClass(this.rootEl, 'visible');
        return;
      }

      dom.addClass(this.rootEl, 'visible');

      dom.empty(this.signatureEl);
      dom.empty(this.docsEl);

      var activeSignature = help.signatures[help.activeSignature];
      if (!activeSignature) {
        return;
      }

      var codeEl = dom.append(this.signatureEl, dom.createElement('div', { className: 'code' }));

      if (activeSignature.parameters.length === 0) {
        var label = dom.append(codeEl, dom.createElement('span'));
        label.textContent = activeSignature.label;
      } else {
        this.renderParams(codeEl, activeSignature, help.activeParameter);
      }

      var activeParam = activeSignature.parameters[help.activeParameter];
      if (activeParam && activeParam.documentation) {
        var documentation = activeParam.documentation;

        var documentationWrapper = dom.createElement('p');
        var documentationEl = dom.append(documentationWrapper, dom.createElement('span', {
          className: 'documentation'
        }));
        documentationEl.textContent = documentation;
        dom.append(this.docsEl, documentationWrapper);
      }

      dom.toggleClass(this.signatureEl, 'has-docs', Boolean(activeSignature.documentation));

      if (activeSignature.documentation) {
        var _documentation = activeSignature.documentation;

        var signatureDocsEl = dom.createElement('p');
        signatureDocsEl.textContent = _documentation;
        dom.append(this.docsEl, signatureDocsEl);
      }
    }
  }, {
    key: 'renderParams',
    value: function renderParams(parent, signature, currentParam) {
      var end = signature.label.length;
      var idx = 0;
      var element = null;

      for (var i = signature.parameters.length - 1; i >= 0; i -= 1) {
        var param = signature.parameters[i];
        idx = signature.label.lastIndexOf(param.label, end - 1);

        var signatureLabelOffset = 0;
        var signatureLabelEnd = 0;

        if (idx >= 0) {
          signatureLabelOffset = idx;
          signatureLabelEnd = idx + param.label.length;
        }

        // non-param part
        element = dom.createElement('span');
        element.textContent = signature.label.substring(signatureLabelEnd, end);
        dom.prepend(parent, element);

        // parem part
        element = dom.createElement('span', {
          className: 'parameter ' + (i === currentParam ? 'active' : '')
        });
        element.textContent = signature.label.substring(signatureLabelOffset, signatureLabelEnd);
        dom.prepend(parent, element);

        end = signatureLabelOffset;
      }

      // end part
      element = dom.createElement('span');
      element.textContent = signature.label.substring(0, end);
      dom.prepend(parent, element);
    }
  }, {
    key: 'setState',
    value: function setState(partialState) {
      this.state = (0, _assign2.default)({}, this.state, partialState);
      this.render();
    }
  }, {
    key: '_createContainerElements',
    value: function _createContainerElements() {
      // container element
      this.rootEl = dom.createElement('div', {
        className: 'CodeMirror-signature-help'
      });

      // signature container element
      this.signatureEl = dom.append(this.rootEl, dom.createElement('div', { className: 'signature' }));

      // docs container element
      this.docsEl = dom.append(this.rootEl, dom.createElement('div', { className: 'docs' }));
    }
  }]);
  return SignatureHelpWidget;
}();

exports.default = SignatureHelpWidget;