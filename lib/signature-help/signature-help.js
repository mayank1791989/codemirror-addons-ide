'use strict';

var _codemirror = require('codemirror');

var _codemirror2 = _interopRequireDefault(_codemirror);

var _SignatureHelp = require('./SignatureHelp');

var _SignatureHelp2 = _interopRequireDefault(_SignatureHelp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var defaultOptions = {
  triggerCharacters: ['('],
  container: null
};

_codemirror2.default.defineOption('signatureHelpOptions', defaultOptions, function (cm, val, old) {
  if (old && old !== _codemirror2.default.Init && cm.state.signatureHelp) {
    cm.state.signatureHelp.destroy();
    cm.state.signatureHelp = null;
  }

  if (val) {
    cm.state.signatureHelp = new _SignatureHelp2.default(cm, val);
  }
});

_codemirror2.default.commands.triggerSignatureHelp = function (cm) {
  if (cm.state.signatureHelp) {
    cm.state.signatureHelp.trigger();
  }
};