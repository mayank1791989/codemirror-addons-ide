'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _SignatureHelpWidget = require('./SignatureHelpWidget');

var _SignatureHelpWidget2 = _interopRequireDefault(_SignatureHelpWidget);

var _throttle = require('../_utils/throttle');

var _dom = require('../_utils/dom');

var dom = _interopRequireWildcard(_dom);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SignatureHelp = function () {
  function SignatureHelp(cm, options) {
    var _this = this;

    (0, _classCallCheck3.default)(this, SignatureHelp);
    this.active = false;
    this.throttledTriggerPending = false;
    this._triggerCharactersMap = null;

    this._handleCursorActivity = function () {
      if (_this.isTriggered()) {
        _this.trigger();
      }
    };

    this._handleChange = function (cm, change) {
      var origin = change.origin,
          text = change.text;

      if (origin !== '+input') {
        return;
      }

      var _text = (0, _slicedToArray3.default)(text, 1),
          typedCharStr = _text[0];

      if (_this._getTriggerCharactersMap()[typedCharStr[0]]) {
        _this.trigger();
      }
    };

    this._throttledTrigger = (0, _throttle.throttle)(function () {
      _this.throttledTriggerPending = false;

      _this._getSignatureHelp().then(function (help) {
        if (!help || !help.signatures || help.signatures.length === 0) {
          _this.cancel();
          return;
        }

        _this.active = true;
        _this.widget.show(help);
        _this._throttledUpdateWidgetPos();
      });
    }, 200);
    this._throttledUpdateWidgetPos = (0, _throttle.rafThrottle)(function () {
      var widgetEl = _this.widget.getElement();
      var pos = _this.cm.cursorCoords(null, 'page');
      dom.position(widgetEl, pos, 'top');
    });

    this.options = options;

    // setup ui
    this.widget = new _SignatureHelpWidget2.default();
    dom.append(dom.getBodyElement(), this.widget.getElement());

    // setup listeners
    this.cm = cm;
    this.cm.on('cursorActivity', this._handleCursorActivity);
    this.cm.on('change', this._handleChange);
  }

  (0, _createClass3.default)(SignatureHelp, [{
    key: 'cancel',
    value: function cancel() {
      this.active = false;
      this._throttledTrigger.cancel();
      this._throttledUpdateWidgetPos();
      this.widget.hide();
    }
  }, {
    key: 'trigger',
    value: function trigger() {
      this.throttledTriggerPending = true;
      this._throttledTrigger();
      this._throttledUpdateWidgetPos();
    }
  }, {
    key: 'isTriggered',
    value: function isTriggered() {
      return this.active || this.throttledTriggerPending;
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      this.cancel();
      this.widget.getElement().remove();
      this.cm.off('cursorActivity', this._handleCursorActivity);
      this.cm.off('change', this._handleChange);
    }
  }, {
    key: '_getSignatureHelp',
    value: function _getSignatureHelp() {
      var provider = this.cm.getHelper(this.cm.getCursor(), 'signature-help');
      return _promise2.default.resolve(provider ? provider(this.cm, this.options) : null);
    }
  }, {
    key: '_getTriggerCharactersMap',
    value: function _getTriggerCharactersMap() {
      if (!this._triggerCharactersMap) {
        this._triggerCharactersMap = this.options.triggerCharacters.reduce(function (acc, char) {
          acc[char] = true;
          return acc;
        }, {});
      }
      return this._triggerCharactersMap;
    }
  }]);
  return SignatureHelp;
}();

exports.default = SignatureHelp;