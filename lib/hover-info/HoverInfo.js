'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _HoverInfoWidget = require('./HoverInfoWidget');

var _HoverInfoWidget2 = _interopRequireDefault(_HoverInfoWidget);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HoverInfo = function HoverInfo(cm, options) {
  (0, _classCallCheck3.default)(this, HoverInfo);
};

exports.default = HoverInfo;