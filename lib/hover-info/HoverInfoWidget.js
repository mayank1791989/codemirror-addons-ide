'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HoverInfoWidget = function () {
  function HoverInfoWidget() {
    (0, _classCallCheck3.default)(this, HoverInfoWidget);

    this._createContainerElements();
  }

  (0, _createClass3.default)(HoverInfoWidget, [{
    key: 'show',
    value: function show(info) {
      this.setState({
        isVisible: true,
        info: info
      });
    }
  }, {
    key: 'hide',
    value: function hide() {
      this.setState({
        isVisible: false,
        info: null
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          info = _state.info,
          isVisible = _state.isVisible;

      if (!isVisible) {
        dom.removeClass(this.rootEl, 'visible');
        return;
      }

      dom.addClass(this.rootEl, 'visible');

      // clean
      dom.empty(this.rootEl);
    }
  }, {
    key: '_createContainerElements',
    value: function _createContainerElements() {
      this.rootEl = dom.createElement('div', {
        className: 'CodeMirror-hover-info'
      });
    }
  }]);
  return HoverInfoWidget;
}();

exports.default = HoverInfoWidget;