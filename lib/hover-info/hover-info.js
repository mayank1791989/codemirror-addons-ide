'use strict';

var _codemirror = require('codemirror');

var _codemirror2 = _interopRequireDefault(_codemirror);

var _HoverInfo = require('./HoverInfo');

var _HoverInfo2 = _interopRequireDefault(_HoverInfo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var defaultOptions = {};

_codemirror2.default.defineOption('hoverInfoOptions', defaultOptions, function (cm, val, old) {
  if (old && old != _codemirror2.default.Init && cm.state.hoverInfo) {
    cm.state.hoverInfo.destroy();
    cm.state.hoverInfo = null;
  }
  if (val) {
    cm.state.hoverInfo = new _HoverInfo2.default(cm, val);
  }
});

_codemirror2.default.commands.showHoverInfo = function (cm) {
  if (cm.state.hoverInfo) {
    cm.state.hoverInfo.trigger();
  }
};