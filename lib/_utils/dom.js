'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addClass = exports.removeClass = undefined;
exports.createElement = createElement;
exports.empty = empty;
exports.append = append;
exports.prepend = prepend;
exports.toggleClass = toggleClass;
exports.position = position;
exports.getBodyElement = getBodyElement;

var _class = require('dom-helpers/class');

var _width = require('dom-helpers/query/width');

var _width2 = _interopRequireDefault(_width);

var _height = require('dom-helpers/query/height');

var _height2 = _interopRequireDefault(_height);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createElement(tag, opts) {
  var el = document.createElement(tag);
  if (opts && opts.className) {
    el.className = opts.className;
  }
  return el;
}
function empty(el) {
  while (el.firstChild) {
    el.removeChild(el.firstChild);
  }
}

function append(parent) {
  for (var _len = arguments.length, children = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    children[_key - 1] = arguments[_key];
  }

  children.forEach(function (child) {
    return parent.appendChild(child);
  });
  return children[children.length - 1];
}

function prepend(parent, child) {
  parent.insertBefore(child, parent.firstChild);
  return child;
}

function toggleClass(el, className, force) {
  if (typeof force === 'boolean') {
    // forceArgPresent
    return force ? (0, _class.addClass)(el, className) : (0, _class.removeClass)(el, className);
  }

  return (0, _class.hasClass)(el, className) ? (0, _class.removeClass)(el, className) : (0, _class.addClass)(el, className);
}

function position(el, refPos, expectedPosition) {
  var winW = (0, _width2.default)(window);
  var winH = (0, _height2.default)(window);
  var elRect = el.getBoundingClientRect();
  var pos = expectedPosition;

  // revert position if not posible to position "expectedPosition"
  if (expectedPosition === 'top') {
    pos = refPos.top - elRect.height < 0 ? 'bottom' : 'top';
  } else if (expectedPosition === 'bottom') {
    pos = refPos.bottom + elRect.height > winH ? 'top' : 'bottom';
  }

  // position 'vertically'
  if (pos === 'top') {
    el.style.top = refPos.top - elRect.height + 'px';
  } else if (pos === 'bottom') {
    el.style.top = refPos.bottom + 'px';
  }

  // position 'horizontally'
  if (refPos.left + elRect.width > winW) {
    // if going out from right than move left to keep inside win
    var shiftLeft = refPos.left + elRect.width - winW;
    el.style.left = refPos.left - shiftLeft + 'px';
  } else {
    el.style.left = refPos.left + 'px';
  }
}

function getBodyElement() {
  if (!document.body) {
    throw new Error('Trying to access document.body but document.body is null.');
  }
  return document.body;
}

exports.removeClass = _class.removeClass;
exports.addClass = _class.addClass;