'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _throttle = require('lodash/throttle');

Object.defineProperty(exports, 'throttle', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_throttle).default;
  }
});
exports.rafThrottle = rafThrottle;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function rafThrottle(fn) {
  var requestId = null;

  var throttled = function throttled() {
    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    if (requestId === null) {
      requestId = window.requestAnimationFrame(function () {
        requestId = null;
        fn.apply(undefined, args);
      });
    }
  };

  throttled.pending = function () {
    return requestId === null;
  };

  throttled.cancel = function () {
    if (requestId) {
      window.cancelAnimationFrame(requestId);
    }
  };

  return throttled;
}