'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = addHintDocsSupport;

require('./hint-docs.css');

var _codemirror = require('codemirror');

var _codemirror2 = _interopRequireDefault(_codemirror);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * NOTE: this will add support to show docs for current
 * selected hint
 */
function addHintDocsSupport(fn) {
  return function (editor, options) {
    var hints = fn(editor, options);

    var hintInfoEl = null;
    var removeHintInfoEl = function removeHintInfoEl() {
      hintInfoEl && hintInfoEl.remove();
    };

    _codemirror2.default.on(hints, 'close', removeHintInfoEl);
    _codemirror2.default.on(hints, 'update', removeHintInfoEl);
    _codemirror2.default.on(hints, 'select', function (hint, hintEl) {
      removeHintInfoEl();
      hintInfoEl = createHintInfoEl(hint);
      var hintsEl = hintEl.parentNode;
      // position next to hints container
      hintInfoEl.style.left = hintsEl.offsetLeft + hintsEl.offsetWidth + 'px';
      hintInfoEl.style.top = hintsEl.offsetTop + 'px';
      hintsEl.parentNode.appendChild(hintInfoEl);
    });

    return hints;
  };
}

function createHintInfoEl(hint) {
  var el = document.createElement('div');
  el.classList.add('CodeMirror-hint-docs');
  el.appendChild(hint.renderDocs ? hint.renderDocs(hint) : document.createTextNode(hint.docs));
  return el;
}