/* @flow */
export { default as throttle } from 'lodash/throttle';

export function rafThrottle<T: Function>(fn: T): T & { cancel: () => void } {
  let requestId = null;

  const throttled = (...args) => {
    if (requestId === null) {
      requestId = window.requestAnimationFrame(() => {
        requestId = null;
        fn(...args);
      });
    }
  };

  throttled.pending = () => {
    return requestId === null;
  };

  throttled.cancel = () => {
    if (requestId) {
      window.cancelAnimationFrame(requestId);
    }
  };

  return (throttled: any);
}
