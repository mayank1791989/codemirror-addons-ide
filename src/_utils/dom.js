/* @flow */
import { hasClass, addClass, removeClass } from 'dom-helpers/class';
import width from 'dom-helpers/query/width';
import height from 'dom-helpers/query/height';

export function createElement(
  tag: string,
  opts?: { className?: string },
): HTMLElement {
  const el = document.createElement(tag);
  if (opts && opts.className) {
    el.className = opts.className;
  }
  return el;
}

export function empty(el: HTMLElement) {
  while (el.firstChild) {
    el.removeChild(el.firstChild);
  }
}

export function append(
  parent: HTMLElement,
  ...children: Array<HTMLElement>
): HTMLElement {
  children.forEach(child => parent.appendChild(child));
  return children[children.length - 1];
}

export function prepend(parent: HTMLElement, child: HTMLElement): HTMLElement {
  parent.insertBefore(child, parent.firstChild);
  return child;
}

export function toggleClass(
  el: HTMLElement,
  className: string,
  force?: boolean,
): void {
  if (typeof force === 'boolean') {
    // forceArgPresent
    return force ? addClass(el, className) : removeClass(el, className);
  }

  return hasClass(el, className)
    ? removeClass(el, className)
    : addClass(el, className);
}

export function position(
  el: HTMLElement,
  refPos: { left: number, top: number, bottom: number },
  expectedPosition: 'bottom' | 'top',
): void {
  const winW = width(window);
  const winH = height(window);
  const elRect = el.getBoundingClientRect();
  let pos = expectedPosition;

  // revert position if not posible to position "expectedPosition"
  if (expectedPosition === 'top') {
    pos = refPos.top - elRect.height < 0 ? 'bottom' : 'top';
  } else if (expectedPosition === 'bottom') {
    pos = refPos.bottom + elRect.height > winH ? 'top' : 'bottom';
  }

  // position 'vertically'
  if (pos === 'top') {
    el.style.top = `${refPos.top - elRect.height}px`;
  } else if (pos === 'bottom') {
    el.style.top = `${refPos.bottom}px`;
  }

  // position 'horizontally'
  if (refPos.left + elRect.width > winW) {
    // if going out from right than move left to keep inside win
    const shiftLeft = refPos.left + elRect.width - winW;
    el.style.left = `${refPos.left - shiftLeft}px`;
  } else {
    el.style.left = `${refPos.left}px`;
  }
}

export function getBodyElement(): HTMLElement {
  if (!document.body) {
    throw new Error(
      'Trying to access document.body but document.body is null.',
    );
  }
  return document.body;
}

export { removeClass, addClass };
