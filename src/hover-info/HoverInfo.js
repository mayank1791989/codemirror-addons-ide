/* @flow */
import HoverInfoWidget from './HoverInfoWidget';
import { type HoverInfoOptions } from './types';

export default class HoverInfo {
  widget: HoverInfoWidget;

  constructor(cm, options: HoverInfoOptions) {}
}
