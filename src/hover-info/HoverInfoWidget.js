/* @flow */

export default class HoverInfoWidget {
  rootEl: HTMLDivElement;

  constructor() {
    this._createContainerElements();
  }

  show(info) {
    this.setState({
      isVisible: true,
      info,
    });
  }

  hide() {
    this.setState({
      isVisible: false,
      info: null,
    });
  }

  render() {
    const { info, isVisible } = this.state;
    if (!isVisible) {
      dom.removeClass(this.rootEl, 'visible');
      return;
    }

    dom.addClass(this.rootEl, 'visible');

    // clean
    dom.empty(this.rootEl);

    
  }

  _createContainerElements() {
    this.rootEl = dom.createElement('div', {
      className: 'CodeMirror-hover-info',
    });
  }
}
