/* @flow */
import CodeMirror from 'codemirror';
import { type HoverInfoOptions } from './types';
import HoverInfo from './HoverInfo';

const defaultOptions: HoverInfoOptions = {};

CodeMirror.defineOption('hoverInfoOptions', defaultOptions, (cm, val, old) => {
  if (old && old != CodeMirror.Init && cm.state.hoverInfo) {
    cm.state.hoverInfo.destroy();
    cm.state.hoverInfo = null;
  }
  if (val) {
    cm.state.hoverInfo = new HoverInfo(cm, val);
  }
});

CodeMirror.commands.showHoverInfo = cm => {
  if (cm.state.hoverInfo) {
    cm.state.hoverInfo.trigger();
  }
};
