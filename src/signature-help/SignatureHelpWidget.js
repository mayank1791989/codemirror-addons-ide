/* @flow */
import { type SignatureHelp, type SignatureInformation } from './types';
import * as dom from '../_utils/dom';

type State = {
  isVisible: boolean,
  help: ?SignatureHelp,
};

export default class SignatureHelpWidget {
  rootEl: HTMLElement;
  signatureEl: HTMLElement;
  docsEl: HTMLElement;

  state: State = {
    isVisible: false,
    help: null,
  };

  constructor() {
    this._createContainerElements();
  }

  show(help: SignatureHelp) {
    this.setState({
      isVisible: true,
      help,
    });
  }

  hide() {
    this.setState({
      isVisible: false,
      help: null,
    });
  }

  getElement() {
    return this.rootEl;
  }

  render() {
    const { help, isVisible } = this.state;
    if (!isVisible || !help) {
      dom.removeClass(this.rootEl, 'visible');
      return;
    }

    dom.addClass(this.rootEl, 'visible');

    dom.empty(this.signatureEl);
    dom.empty(this.docsEl);

    const activeSignature = help.signatures[help.activeSignature];
    if (!activeSignature) {
      return;
    }

    const codeEl = dom.append(
      this.signatureEl,
      dom.createElement('div', { className: 'code' }),
    );

    if (activeSignature.parameters.length === 0) {
      const label = dom.append(codeEl, dom.createElement('span'));
      label.textContent = activeSignature.label;
    } else {
      this.renderParams(codeEl, activeSignature, help.activeParameter);
    }

    const activeParam = activeSignature.parameters[help.activeParameter];
    if (activeParam && activeParam.documentation) {
      const { documentation } = activeParam;
      const documentationWrapper = dom.createElement('p');
      const documentationEl = dom.append(
        documentationWrapper,
        dom.createElement('span', {
          className: 'documentation',
        }),
      );
      documentationEl.textContent = documentation;
      dom.append(this.docsEl, documentationWrapper);
    }

    dom.toggleClass(
      this.signatureEl,
      'has-docs',
      Boolean(activeSignature.documentation),
    );

    if (activeSignature.documentation) {
      const { documentation } = activeSignature;
      const signatureDocsEl = dom.createElement('p');
      signatureDocsEl.textContent = documentation;
      dom.append(this.docsEl, signatureDocsEl);
    }
  }

  renderParams(
    parent: HTMLElement,
    signature: SignatureInformation,
    currentParam: number,
  ): void {
    let end = signature.label.length;
    let idx = 0;
    let element: ?HTMLElement = null;

    for (let i = signature.parameters.length - 1; i >= 0; i -= 1) {
      const param = signature.parameters[i];
      idx = signature.label.lastIndexOf(param.label, end - 1);

      let signatureLabelOffset = 0;
      let signatureLabelEnd = 0;

      if (idx >= 0) {
        signatureLabelOffset = idx;
        signatureLabelEnd = idx + param.label.length;
      }

      // non-param part
      element = dom.createElement('span');
      element.textContent = signature.label.substring(signatureLabelEnd, end);
      dom.prepend(parent, element);

      // parem part
      element = dom.createElement('span', {
        className: `parameter ${i === currentParam ? 'active' : ''}`,
      });
      element.textContent = signature.label.substring(
        signatureLabelOffset,
        signatureLabelEnd,
      );
      dom.prepend(parent, element);

      end = signatureLabelOffset;
    }

    // end part
    element = dom.createElement('span');
    element.textContent = signature.label.substring(0, end);
    dom.prepend(parent, element);
  }

  setState(partialState: $Shape<State>) {
    this.state = {
      ...this.state,
      ...partialState,
    };
    this.render();
  }

  _createContainerElements() {
    // container element
    this.rootEl = dom.createElement('div', {
      className: 'CodeMirror-signature-help',
    });

    // signature container element
    this.signatureEl = dom.append(
      this.rootEl,
      dom.createElement('div', { className: 'signature' }),
    );

    // docs container element
    this.docsEl = dom.append(
      this.rootEl,
      dom.createElement('div', { className: 'docs' }),
    );
  }
}
