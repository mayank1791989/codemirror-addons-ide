/* @flow */
import CodeMirror from 'codemirror';
import { type SignatureHelpOptions } from './types';
import SignatureHelp from './SignatureHelp';

const defaultOptions: SignatureHelpOptions = {
  triggerCharacters: ['('],
  container: null,
};

CodeMirror.defineOption(
  'signatureHelpOptions',
  defaultOptions,
  (cm, val, old) => {
    if (old && old !== CodeMirror.Init && cm.state.signatureHelp) {
      cm.state.signatureHelp.destroy();
      cm.state.signatureHelp = null;
    }

    if (val) {
      cm.state.signatureHelp = new SignatureHelp(cm, val);
    }
  },
);

CodeMirror.commands.triggerSignatureHelp = cm => {
  if (cm.state.signatureHelp) {
    cm.state.signatureHelp.trigger();
  }
};
