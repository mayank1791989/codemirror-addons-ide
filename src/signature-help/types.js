/* @flow */
/**
 * Represents a parameter of a callable-signature. A parameter can
 * have a label and a doc-comment.
 */
export type ParameterInformation = {
  /**
   * The label of this signature. Will be shown in
   * the UI.
   */
  +label: string,
  /**
   * The human-readable doc-comment of this signature. Will be shown
   * in the UI but can be omitted.
   */
  +documentation?: string,
};

/**
 * Represents the signature of something callable. A signature
 * can have a label, like a function-name, a doc-comment, and
 * a set of parameters.
 */
export type SignatureInformation = {
  /**
   * The label of this signature. Will be shown in
   * the UI.
   */
  +label: string,
  /**
   * The human-readable doc-comment of this signature. Will be shown
   * in the UI but can be omitted.
   */
  +documentation?: string,
  /**
   * The parameters of this signature.
   */
  +parameters: Array<ParameterInformation>,
};
/**
 * Signature help represents the signature of something
 * callable. There can be multiple signatures but only one
 * active and only one active parameter.
 */
export type SignatureHelp = {
  /**
   * One or more signatures.
   */
  +signatures: Array<SignatureInformation>,
  /**
   * The active signature.
   */
  +activeSignature: number,
  /**
   * The active parameter of the active signature.
   */
  +activeParameter: number,
};

export type SignatureHelpOptions = {
  +triggerCharacters: Array<string>,
};
