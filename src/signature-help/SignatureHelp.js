/* @flow */
import SignatureHelpWidget from './SignatureHelpWidget';
import { type SignatureHelpOptions } from './types';
import { throttle, rafThrottle } from '../_utils/throttle';
import * as dom from '../_utils/dom';

type CodemirrorInstance = any;

export default class SignatureHelp {
  active: boolean = false;
  throttledTriggerPending: boolean = false;
  options: SignatureHelpOptions;
  widget: SignatureHelpWidget;
  cm: CodemirrorInstance;

  _triggerCharactersMap = null;

  constructor(cm: CodemirrorInstance, options: SignatureHelpOptions) {
    this.options = options;

    // setup ui
    this.widget = new SignatureHelpWidget();
    dom.append(dom.getBodyElement(), this.widget.getElement());

    // setup listeners
    this.cm = cm;
    this.cm.on('cursorActivity', this._handleCursorActivity);
    this.cm.on('change', this._handleChange);
  }

  cancel() {
    this.active = false;
    this._throttledTrigger.cancel();
    this._throttledUpdateWidgetPos();
    this.widget.hide();
  }

  trigger() {
    this.throttledTriggerPending = true;
    this._throttledTrigger();
    this._throttledUpdateWidgetPos();
  }

  isTriggered() {
    return this.active || this.throttledTriggerPending;
  }

  destroy() {
    this.cancel();
    this.widget.getElement().remove();
    this.cm.off('cursorActivity', this._handleCursorActivity);
    this.cm.off('change', this._handleChange);
  }

  _handleCursorActivity = () => {
    if (this.isTriggered()) {
      this.trigger();
    }
  };

  _handleChange = (cm: CodemirrorInstance, change: any) => {
    const { origin, text } = change;
    if (origin !== '+input') {
      return;
    }

    const [typedCharStr] = text;
    if (this._getTriggerCharactersMap()[typedCharStr[0]]) {
      this.trigger();
    }
  };

  _getSignatureHelp(): Promise<any> {
    const provider = this.cm.getHelper(this.cm.getCursor(), 'signature-help');
    return Promise.resolve(provider ? provider(this.cm, this.options) : null);
  }

  _getTriggerCharactersMap() {
    if (!this._triggerCharactersMap) {
      this._triggerCharactersMap = this.options.triggerCharacters.reduce(
        (acc, char) => {
          acc[char] = true;
          return acc;
        },
        {},
      );
    }
    return this._triggerCharactersMap;
  }

  _throttledTrigger = throttle(() => {
    this.throttledTriggerPending = false;

    this._getSignatureHelp().then(help => {
      if (!help || !help.signatures || help.signatures.length === 0) {
        this.cancel();
        return;
      }

      this.active = true;
      this.widget.show(help);
      this._throttledUpdateWidgetPos();
    });
  }, 200);

  _throttledUpdateWidgetPos = rafThrottle(() => {
    const widgetEl = this.widget.getElement();
    const pos = this.cm.cursorCoords(null, 'page');
    dom.position(widgetEl, pos, 'top');
  });
}
