/* @flow */
import './hint-docs.css';
import CodeMirror from 'codemirror';

/**
 * NOTE: this will add support to show docs for current
 * selected hint
 */
export default function addHintDocsSupport(fn) {
  return (editor, options) => {
    const hints = fn(editor, options);

    let hintInfoEl = null;
    const removeHintInfoEl = () => {
      hintInfoEl && hintInfoEl.remove();
    };

    CodeMirror.on(hints, 'close', removeHintInfoEl);
    CodeMirror.on(hints, 'update', removeHintInfoEl);
    CodeMirror.on(hints, 'select', (hint, hintEl) => {
      removeHintInfoEl();
      hintInfoEl = createHintInfoEl(hint);
      const hintsEl = hintEl.parentNode;
      // position next to hints container
      hintInfoEl.style.left = `${hintsEl.offsetLeft + hintsEl.offsetWidth}px`;
      hintInfoEl.style.top = `${hintsEl.offsetTop}px`;
      hintsEl.parentNode.appendChild(hintInfoEl);
    });

    return hints;
  };
}

function createHintInfoEl(hint) {
  const el = document.createElement('div');
  el.classList.add('CodeMirror-hint-docs');
  el.appendChild(
    hint.renderDocs
      ? hint.renderDocs(hint)
      : document.createTextNode(hint.docs),
  );
  return el;
}
